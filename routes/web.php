<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\DatabaseNotifyController;
use App\Http\Controllers\SMSController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::view('/registration','registration');
Route::view('/logination','/logination');
Route::get('/', function () {
    return view('welcome');
});
Auth::routes();
Route::get('/users',[UserController::class,'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/showtable',[UserController::class,'showTable']);
Route::get('/ajax',[UserController::class,'userDatatable'])->name('users.ajax');
Route::get('/users/delete/{id}',[UserController::class,'delete'])->name('users.delete');
Route::view('sendmail','mailForm')->name('sendmail');
Route::post('sendmail',[MailController::class,'sendMail'])->name('sendmail.post');
Route::view('addmember','employee.AddEmployee');
Route::post('addmember',[EmployeeController::class,'store'])->name('addmember');
// Route::view('edit\{id}','employee.EditEmployee');
Route::get('showmember',[EmployeeController::class,'index']);
Route::get('showmember/edit/{id}',[EmployeeController::class,'edit']);
Route::put('showmember/edit/{id}',[EmployeeController::class,'update'])->name('mem.update');
Route::delete('showmember/delete/{id}',[EmployeeController::class,'destroy']);
Route::get('showmember/subscription/{id}',[EmployeeController::class,'subscription']);
// Route::resource('employee',EmployeeController::class);
route::view('time','time');
Route::get('/event',[EventController::class,'EventSendMail']);
Route::get('/notify',[NotificationController::class,'index']);
Route::get('followNotify/{id}',[DatabaseNotifyController::class,'index'])->name('followNotify');
Route::get('markasread/{id}',[DatabaseNotifyController::class,'markasread'])->name('markasread');
Route::get('sms',[SMSController::class,'index']);
// Route::get('/notify',[NotificationController::class,'index']);


// Route::get('phpinfo', fn () => phpinfo());