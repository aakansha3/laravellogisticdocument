<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group([
    'middleware' => 'api',
    'prefix' => 'v1/auth'
], function ($router) {    
    Route::post('registeration',[App\Http\Controllers\Api\v1\AuthController::class,'register'])->name('registration');
    Route::post('logination',[App\Http\Controllers\Api\v1\AuthController::class,'login'])->name('logination');
    Route::get('profile',[App\Http\Controllers\Api\v1\AuthController::class,'profile'])->name('profile');
    Route::post('logoution',[App\Http\Controllers\Api\v1\AuthController::class,'logout'])->name('logoution');
    Route::post('refresh_access_token',[App\Http\Controllers\Api\v1\AuthController::class,'refreshAccessToken'])->name('refresh_access_token');

});
// Route::group([
//     'middleware' => 'api',
//     'prefix' => 'v2/auth'
// ], function ($router) { 
//     Route::post('registeration',[App\Http\Controllers\Api\v2\AuthController::class,'register'])->name('registration');
//     Route::post('logination',[App\Http\Controllers\Api\v2\AuthController::class,'login'])->name('logination');
//     Route::get('profile',[App\Http\Controllers\Api\v2\AuthController::class,'profile'])->name('profile');
//     Route::post('logoution',[App\Http\Controllers\Api\v2\AuthController::class,'logout'])->name('logoution');
// });
