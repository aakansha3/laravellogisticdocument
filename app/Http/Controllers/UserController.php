<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use DataTables;
class UserController extends Controller
{
    //
    public function showTable(){
        $user = User::all();
        return view('showtable',['users'=>$user]);
    }  
    public function index(Request $request){              
        return view('users.index');
    }

    public function userDatatable(Request $request){        
        $data = User::select('*');
        return DataTables::of($data)->addIndexColumn()->make(true);        
    }

    public function delete($id){                
        $user = User::find($id);
        $user->delete();
        return redirect()->route('users.index');
    }   
}
