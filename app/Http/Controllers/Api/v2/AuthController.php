<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Member;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Auth;
use Validator;

class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api',['only'=>['profile']]);
    }
    
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|string|email|unique:members',
            'password' => 'required|string|confirmed|min:6',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);            
        }
        $member = Member::create(array_merge(
            $validator->validated(),
            ['password'=>bcrypt($request->password)]
        ));
        $successMessage = "Message: User successfully registered\n";
    $memberDetails = "Name: " . $member->name . "\nEmail: " . $member->email . "\n";
    return response($successMessage . $memberDetails, 201)->header('Content-Type', 'text/plain');
    }
    
    public function login(Request $request){
        $validator = Validator::make($request->all(),[            
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }
        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    protected function createNewToken($token){
        $credentails = "token: " . $token . "\ntoken_type: bearer\nexpires_in".auth()->factory()->getTTL() * 60 . "\n";
        return response($credentails,201)->header('Content-Type', 'text/plain');        
    }  
    
    public function profile(){           
        $user = auth()->user();
        $name = $user->name;
        $email = $user->email; 
        return response("Name:" . $name . "\nEmail: " .$email. "\n")->header('Content-Type', 'text/plain');       
    }
    
    public function logout(){
        auth()->logout();
        return response('message: Successfully logged out')->header('Content-Type', 'text/plain');
    }
}
