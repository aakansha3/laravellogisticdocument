<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Notification;
use App\Models\User;
use App\Notifications\SMSNotification;
class SMSController extends Controller
{
    //
    public function index(){
        $user =  User::first();
        $user->notify(new SMSNotification($user));
        dd('Notification sent!');
    }
}