<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Models\Member;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use Auth;
use Validator;
class AuthController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth:api',['only'=>['profile']]);
    }
    /**
 * Add a new user.
 *
 * @OA\Post(
 *     path="/api/auth/registeration",
 *     tags={"user"},
 *     operationId="register",
 *     summary="Add a new user",
 *     description="Creates a new user with the provided information.",  
 *     @OA\RequestBody(
 *         required=true,
 *         description="User data",
 *         @OA\JsonContent(
 *             required={"name", "email", "password", "password_confirmation"},
 *             @OA\Property(property="name", type="string", example="John Doe"),
 *             @OA\Property(property="email", type="string", format="email", example="john@example.com"),
 *             @OA\Property(property="password", type="string", format="password", example="secret"),
 *             @OA\Property(property="password_confirmation", type="string", format="password", example="secret")
 *         )
 *     ),
 *    @OA\Response(
 *     response=200,
 *     description="User added successfully",
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(
 *             property="result",
 *             type="string",
 *             example="User added successfully"
 *         )
 *     )
 *   ),    
 *     @OA\Response(
 *         response=422,
 *         description="Validation error",
 *         @OA\JsonContent(
 *         type="object",
 *         @OA\Property(
 *             property="email",
 *             type="string",
 *             example="email has already taken."
 *         )
 *     )         
 *     ),      
 * )
 */
    public function register(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|string|email|unique:members',
            'password' => 'required|string|confirmed|min:6',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);            
        }
        $member = Member::create(array_merge(
            $validator->validated(),
            ['password'=>bcrypt($request->password)]
        ));
        return response()->json([
            'message' => 'User successfully registered',
            'member' => $member,
        ],201);
    }
    /**
     * @OA\POST(
     *     path="/api/auth/logination",
     *     tags={"user"},
     *     summary="Logs user into system",
     *     operationId="login",
     *     @OA\RequestBody(
 *         required=true,
 *         description="User data",
 *         @OA\JsonContent(
 *             required={"email", "password"}, *             
 *             @OA\Property(property="email", type="string", format="email", example="john@example.com"),
 *             @OA\Property(property="password", type="string", format="password", example="secret"),             
 *         )
 *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",         
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="result",type="string",example="User Logged in Successfully"),
     *         ),         
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid username/password supplied",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Email or password do not match"),
     *         ),
     *     )
     * )
     */
    public function login(Request $request){
        $validator = Validator::make($request->all(),[            
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(),422);
        }
        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }
    protected function createNewToken($token){
        $user = Auth::user();
        $refreshToken = $this->generateRefreshToken($user);
        return response()->json([
            'access_token' => $token,
            'refresh_token' => $refreshToken,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,            
        ]);
    }  
    /**
     * @OA\Get(
     *     path="/api/auth/profile",
     *     tags={"user"},
     *     summary="Profile page",
     *     operationId="profile",     
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",         
     *         @OA\JsonContent(
 *     type="array",
 *     @OA\Items(
 *         type="object",
 *         @OA\Property(
 *             property="id",
 *             type="integer",
 *             example="id"
 *         ),
 *         @OA\Property(
 *             property="name",
 *             type="string",
 *             example="name"
 *         ),
 *         @OA\Property(
 *             property="email",
 *             type="string",
 *             example="email"
 *         ),
 *         @OA\Property(
 *             property="password",
 *             type="string",
 *             example="password"
 *         )
 *     )
 * )         
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid User",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Unauthorised User"),
     *         ),
     *     )
     * )
     */  
    public function profile(){           
        return response()->json(auth()->user());        
    }
    /**
     * @OA\POST(
     *     path="/api/auth/logoution",
     *     tags={"user"},
     *     summary="Logout User from system",
     *     operationId="logout",     
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",         
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="result",type="string",example="User Successfully Logged out from System"),
     *         ),         
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Operation Failed",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error",type="string",example="Failed to Logout"),
     *         ),
     *     )
     * )
     */
    public function logout(){
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }    
}