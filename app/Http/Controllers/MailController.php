<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Mail\SendingMail;
class MailController extends Controller
{
    //
    public function sendMail(Request $req){
        $to = $req->input('to');
        $subject = $req->input('subject');
        $message = $req->input('message');
        Mail::to($to)->queue(new SendingMail($subject,$message));
        Session::flash('success', 'Email sent successfully!');
        return redirect()->route('sendmail');
    }
}
