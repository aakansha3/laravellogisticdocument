<?php

namespace App\Http\Controllers;
use App\Notifications\UserFollowNotification;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Notification;
class DatabaseNotifyController extends Controller
{
    public function index($id){
        $userToFollow = User::find($id);
        $follower = auth()->user();
        $userToFollow->notify(new UserFollowNotification($follower));
        $users = User::all(); 
        
        return view('home', ['users' => $users]);
        
        // if(auth()->user()){
        //     $user = User::find(12);
                  
        //     auth()->user()->notify(new UserFollowNotification($user));
        // }
        // $user = User::find(8);
        // // dd(auth()->user());
        // dd(new UserFollowNotification($user));
        // dd('done');
        
    }
    public function markasread($id){
        // dd($id);
        if($id){
            auth()->user()->unreadNotifications->where('id',$id)->markAsRead();
        }
        return back();
    }
    
}
