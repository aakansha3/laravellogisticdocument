<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $employees = Employee::all();
        // return $employees;
        
        return View::make('employee.ShowEmployee')
            ->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>'required | min:5',
            'email'=>'required | email:rfc,dns',
            'password' => 'required | min:8'
        ]);
        $password = Hash::make($request->password);
        $employee = new Employee;
        $employee->name=$request->name;
        $employee->email=$request->email;
        $employee->password=$password;
        $employee->save();        
        // return $data;                
        return "Employee Added Succesfully";
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //    
        
        $employees = Employee::find($id);
        // return $employees;
        
        return View::make('employee.EditEmployee')
            ->with('employees', $employees);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        
        //
        $request->validate([
            'name'=>'required | min:5',
            'email'=>'required | email:rfc,dns',
            'password' => 'required | min:8'
        ]); 
        $employees = Employee::find($id);       
        $employees->name=$request->name;
        $employees->email=$request->email;
        $employees->password=$request->password;
        $employees->save();
        return "Employee Edited Succesfully";
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $employees = Employee::find($id);
        $employees->delete();
        return "deleted Succesfully";
    }
    public function subscription(string $id){
        $employees = Employee::find($id);
        // echo $employees->name1;
        $fullname = $employees->getFullNameAttribute($employees->firstname,$employees->lastname);
        $subscriptionEndDate = $employees->calculateSubscriptionEndDate();
        return view('employee.subscription',['subscriptionEndDate' => $subscriptionEndDate,'employees' => $employees,'fullname'=>$fullname]);
    }
}
