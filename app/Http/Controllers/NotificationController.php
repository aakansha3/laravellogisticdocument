<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Notifications\WelcomeNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(){
        $user = User::find(8);        
        // Notification::send($user,new WelcomeNotification());    
        $user->notify(new WelcomeNotification());    
        dd('done');
    }
}
