<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
class Employee extends Model
{
    use HasFactory;    
    public function getNameAttribute($value) {
        return ucfirst($value);
    }
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }
    public function getFullNameAttribute($value){
        return  $this->firstname . " " . $this->lastname;
    }
    // public function getName1Attribute($value) {
    //     return ucfirst($value) . " " . $this->email;
    // }
    // public function setEmailAttribute($value) {
    //     $this->attributes['email'] = strtolower($value);
    // }
    protected function Email(): Attribute{
        return Attribute::make(
            set: fn($value) => strtolower($value)
        );
    }
    
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y  h:i:s A');
    }

    public function addSubscriptionDays($noOfDays){        
        $this->subscription_end_date = $this->subscription_end_date->addDays($noOfDays);
        $this->save();
    }


    public function calculateSubscriptionEndDate(){        
        $createdDate = Carbon::parse($this->created_at); 
        $subscriptionEndDate = $createdDate->copy()->addDays(7);
        return $subscriptionEndDate;
    }
}
