<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Creating .env file

command = touch .env (for creating .env file)

## Generating key 

command = php artisan key:generate

## chmod for cache and storage

command = chmod -R 775 storage
command = chmod -R 775 storage/framework/cache

## creating controller

php artisan make:controller UserController

## DataTables Using Yajra 

Step 1 :- install yajra DataTables using this command   "composer require yajra/laravel-datatables"
Step 2 :- GoTo To config\app.php and in Provider write this line "\Yajra\Datatables\DataTablesServiceProvider::class" and in aliases write this line "'DataTables' =>  \Yajra\DataTables\Facades\DataTables::class"
Step 3 :- php artisan vendor:publish --provider="Yajra\DataTables\DataTablesServiceProvider"
Step 4 :- php artisan datatables:make User (This will create folder DataTables\UserDataTables.php)
Step 5 :- inside UsersDataTables class method called getColumns is there in that add field name you want from your database.ex.Column::make('id')
Step 6 :- Inside Controller import UserDataTables "use App\DataTables\UserDataTable;" and add this function 
"public function index(UserDataTable $datatable){        
        return $datatable->render('users.index');
    }" 
this function will render data to view.
Step 7 :- Make folder users\index.blade.php in view folder inside that "{!! $dataTable->table(['class'=>'table table-bordered'],true) !!}" and "{!!$dataTable->scripts()!!}"

## Sending Mail Using Form

Step 1 :- Make Form in view file (here mailForm.blade.php).
Step 2 :- Make Route to view this Form also make post route for this form.
Step 3 :- Make Controller "php artisan make:controller MailController".
Step 4 :- Make Mailable class "php artisan make:mail SendingMail".
Step 5 :- In Mail Controller import Mail,Session and Mailable class.
Step 6 :- In Mail Controller Request data that was enter by user and send mail and pass data to Mailable class.
Step 7 :- In Mail class construct method will take data passed from controller,envelope method passed subject and content will pass data to view (here email.demoMail).
Step 8 :- If using queue method for sending mail then need to create table jobs "php artisan make:migration create_jobs_table".
Step 9 :- then migrate table "php artisan migrate".
Step 10 :- php artisan queue:work.

## Sending Mail on User delete with Observer

Step 1 :- Make Observer "php artisan make:observer UserObserver --model=User"
Step 2 :- delete method is already there so in UserController $user is passed to Model and Model pass data to UserObserver.
Step 3 :- Inside UserObserver import Mail,Mail class(here deleteMail.php)
Step 4 :- Inside delete method of UserObserver write "Mail::to($user->email)->queue(new DeleteMail($user));" this will send mail and pass $user to Mail class.
Step 5 :- In mail class construct method will get $user and build method will pass data to view(here email.deleteMail).
Step 6 :- In Provider\EventServiceProvider in boot method add this line "User::observe(UserObserver::class);" any changes in User will be observe by UserObserver.

## Adding column to existing table without losing data

php artisan make:migration add_referenceno_columns_to_employees_table --table=employee

## Accessor and Mutators

Creating method inside model ,accessor mean getting data from database and showing it in different format than database.
Mutators means getting input from user and input storing in different format in database.

## Renaming column_name in the table

php artisan make:migration rename_author_id_in_posts_table --table=posts

It will create one migration and then replace up() and down() functions with this in that created migration,

public function up()
{
    Schema::table('posts', function (Blueprint $table) {
        $table->renameColumn('author_ID', 'user_id');
    });
}
public function down()
{
    Schema::table('posts', function (Blueprint $table) {
        $table->renameColumn('user_id', 'author_ID');
    });
}
## Api Documentation using Darkaonline

Step 1 :- composer require darkaonline/l5-swagger
Step 2 :- open your config/app.php and add this line in providers section
            L5Swagger\L5SwaggerServiceProvider::class,
Step 3 :- You can access your documentation at /api/documentation endpoint. 

## Event Listener 

Step 1 :- php artisan make:event SendMail
Step 2 :- php artisan make:listener SendMailFired --event="SendMail"
Step 3 :- php artisan make:controller EventController
Controller will pass data to Event SendMail.
SendMail will take data in construct method.
In Listener(SendMailFired) of Event(SendMail) Find that User and Send mail to that user.
In Provider/EventServiceProvider.php
import Event and Listener and Register that Event 

## Notification using Mail

Step 1 :- php artisan make:notification WelcomeNotification
Step 2 :- php artisan make:controller NotificationController
controller will pass data to notification class and notication class will send mail

## Notification with Database

Step 1 :- php artisan make:controller DatabaseNotifyController
Step 2 :- php artisan make:notification UserFollowNotification
in controller if condition will check for user if it is authenticated then it will notify 
$user is containing specific user that is passes to notification class UserFollowNotification
UserFollowNotification will take user in construct method and via method write database instead of mail
export data from toArray method
in home.blade.php in foreach loop will check for notification for authenticated user if there will be then it will show