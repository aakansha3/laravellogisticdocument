<div class="container">
<div style="display: inline-block;">
<a href="#" class="btn btn-sm btn-primary">Edit</a>
</div>
<div style="display: inline-block;">
<a href="{{ route('users.delete',$user->id) }}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this user?')">Delete</a>
</div>
</div>

