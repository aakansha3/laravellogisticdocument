<!DOCTYPE html>
<html>
<head>
    <title>Your DataTable</title>
    <!-- Include necessary CSS and JS files -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
</head>
<body>
<h2 align="center" class="my-2">Laravel 10 Implementing Yajra Datatables</h2>
    <div class="container my-5">        
        <table id="myTable" class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>Sr No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>                
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            var table = $('#myTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.ajax') }}",
                columns: [
                    { data: 'id' },
                    { data: 'name' },
                    { data: 'email' },
                    { 
                    data: null,
                    render: function(data) {
                        return '<a href="#" class="btn btn-success btn-sm">Edit</a>&nbsp;<a href="/users/delete/' + data.id + '" class="btn btn-danger btn-sm">Delete</a>';
                    }
                }
                ]
            });
        });
    </script>
</body>
</html>
