<!DOCTYPE html>
<html>
<head>
    <title>Mail Form</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<h1>Send Mail</h1>
@if(Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif
 

{{Form::open(array('route'=>'sendmail.post','method'=>'post'))}}
@csrf    
<div class="form-group">
        {{ Form::label('to', 'To')}}
        {{ Form::email('to', null, array('class' => 'form-control','placeholder'=>"Send Mail to")) }}
    </div>
    <div class="form-group">
        {{ Form::label('subject', 'Subject') }}
        {{ Form::text('subject', null, array('class' => 'form-control','placeholder'=>'Subject')) }}
    </div>
    <div class="form-group">
        {{ Form::label('message', 'Message') }}
        {{ Form::text('message', null, array('class' => 'form-control','placeholder'=>'Message')) }}
    </div>
    {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
</div>
</body>
</html>
