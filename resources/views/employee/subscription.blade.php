<!DOCTYPE html>
<html>
<head>
    <title>Shark App</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">

<h1>Showing {{ $employees->fullname }}</h1>

    <div class="jumbotron text-center">
        <h2>Your Subscription End on :-</h2>
        <h2>{{$subscriptionEndDate}}</h2>         
    </div>

</div>
</body>
</html>
