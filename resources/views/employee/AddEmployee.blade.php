<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
    </script>
    <style>
    ul {
        color: red;
        /* Set text color to red */
        list-style-type: none;
        /* Remove default list bullets */
        padding: 0;
        /* Remove default padding */
    }
    </style>
</head>

<body>
    <div class="container my-5">
        <h1>Add Employee</h1>

        {{ HTML::ul($errors->all()) }}

        {{ Form::open(array('url' => 'addmember')) }}

        <div class="form-group">
            {{ Form::label('name', 'Name') }}
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>
        <span style="color:red">@error('name'){{$message}}@enderror</span>
        
        <div class="form-group">
            {{ Form::label('email', 'Email') }}
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>
        <span style="color:red">@error('email'){{$message}}@enderror</span>        
        <div class="form-group">
            {{ Form::label('password', 'password') }}
            {{ Form::text('password', null, array('class' => 'form-control')) }}
        </div>
        <span style="color:red">@error('password'){{$message}}@enderror</span>    
        <div class="form-group">
            {{ Form::label('subscriptiondays', 'Subscription Days') }}
            {{ Form::text('subscriptiondays', null, array('class' => 'form-control')) }}
        </div>
        <span style="color:red">@error('password'){{$message}}@enderror</span> 
        <br>
        {{ Form::submit('Add Employee', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
    </div>
</body>

</html>