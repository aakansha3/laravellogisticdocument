<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous">
    </script>
</head>
<body>
<div class="container my-5">
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>           
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($employees as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>            

            <!-- we will also add show, edit, and delete buttons -->
            <td>    
            <!-- {{ Form::open(array('url' => 'showmember/delete/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}             -->
                <!-- show the shark (uses the show method found at GET /sharks/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('showmember/edit/'. $value->id) }}">Edit</a>
                <a class="btn btn-small btn-danger" href="{{ URL::to('showmember/delete/'. $value->id) }}">Delete</a>
                <a class="btn btn-small btn-primary" href="{{ URL::to('showmember/subscription/'. $value->id) }}">Subscription</a>
                <!-- edit this shark (uses the edit method found at GET /sharks/{id}/edit -->
                <!-- <a class="btn btn-small btn-danger" href="{{ URL::to('showmember/delete/'. $value->id) }}">Delete</a> -->

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

</body>
</html>