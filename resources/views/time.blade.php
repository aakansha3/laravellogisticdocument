@php
use Carbon\Carbon;
use Carbon\CarbonTimeZone;
$current = Carbon::now();
echo $current."<br>" ;
$today = Carbon::today();
echo $today."<br>";
$newYear = new Carbon('first day of January 2016');
echo $newYear."<br>";
// get the current time
$current = Carbon::now();
echo $current."<br>" ;
// add 30 days to the current time
$trialExpires = $current->addDays(1);
echo $trialExpires."<br>" ;
$tz = new CarbonTimeZone('Asia/Kolkata');
echo $tz."<br>" ;
$dtToronto = Carbon::create(2012, 1, 1, 0, 0, 0, 'America/Toronto');
$dtVancouver = Carbon::create(2012, 1, 1, 0, 0, 0, 'Asia/Kolkata');
echo $dtVancouver->diffInHours($dtToronto)."<br>" ;
$nextsunday = Carbon::parse('next sunday');
echo $nextsunday."<br>" ;
@endphp