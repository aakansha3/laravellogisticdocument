@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Suggestion Start following') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach($users as $user)
                    <div class="alert alert-primary">
                        Start following {{$user['name']}}
                        @if($user->isFollowing)
                        <button class="btn btn-secondary" disabled>Following</button>
                        @else
                        <a class="btn btn-success" href="{{ route('followNotify',$user->id) }}">Follow</a>
                        @endif
                        <br />
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Notification') }}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @foreach(auth()->user()->unreadnotifications as $notification)
                    <div class="alert alert-primary">
                        {{$notification->data['name']}} started following you!!!
                        <a class="btn btn-primary" href="{{route('markasread',$notification->id)}}">Mark as read</a>
                        <br />
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<div class="container">